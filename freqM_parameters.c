/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * @author  Ivan Кувалда(Sledgehammer) Kuvaldin <mailto:i.kyb@ya.ru>
 */

#include <stdint.h>
#include "./freqM.h"
#include "Sledge/bsp.h"
#include "Sledge/drv/stm32/sledge_stm32_timers.h"
#include "Sledge/c11n/c11n.h"


/** Базовая частота таймеров. Может быть использовано для понижения эталонной частоты 
  * частото-измерительного модуля во время выполнения. 
  * Пременные с суффиксом _cached используются функцией @ref parameters_listener() для
  * отслеживания изменения соответствующих переменных извне (напр. отладчика или CLI) */
/*volatile*/ uint32_t freqM_timfreqbase = freqM_defaultTIM_REF_BASE_FREQ;															
static uint32_t timfreqbase_cached = freqM_defaultTIM_REF_BASE_FREQ;													
/** Аппаратный фильтр таймеров STM32. Подробности в Ref.Man.p.614 "External trigger filter"
  * @note Тут нужно учесть, что частота сэмплирования фильтра завистит от базовой частоты 
  * таймера и если значение фильтра больше 0x3, то еще от TIM_ClockDivision. 
  * Т.е если TIM4, сидящий на APB1, имеет Fck_int=84MHz и фильтр 1(или 2), то для TIM1, 
  * работающего на частоте Fck_int=168MHz фильтр должен быть 2(или 3) соответственно 	*/	
/*volatile*/ uint16_t freqM_hwInputFilter 		 = freqM_defaultHW_INPUT_FILTER;	// Значение для TimExtCounter.SMCR.ETF и TimRef.inCap1.filter
static uint16_t hwInputFilter_cached = freqM_defaultHW_INPUT_FILTER;	// Не рекомендуется более 0x3
/// Предделитель входного сигнала (импульсов)
/*volatile*/ uint16_t freqM_hwInputPrescaler = freqM_defaultHW_INPUT_PRESCALER;
static uint16_t hwInputPrescaler_cached = freqM_defaultHW_INPUT_PRESCALER;
// Частота дискретизации, период дискретизации
/*volatile*/ uint16_t freqM_sampleRate = freqM_defaultSAMPLE_RATE;
static uint16_t sampleRate_cached = freqM_defaultSAMPLE_RATE;
static uint16_t sampleRatePeriod;//s = defaultTIM_REF_BASE_FREQ / defaultSAMPLE_RATE;//0xffff;


extern uint16_t prescaler( TIM_TypeDef* tim, uint32_t freq );		// Расчет предделителя для конкретного таймера по заданной частоте



const Parameter_t freqM_PARAMETERS[] = {
	{"hwInputFilter"   , NULL, C11N_UINT16, (setterFunction_t)    freqM_hwInputFilter_set, (getterFunction_t)    freqM_hwInputFilter_get, 0,0,    freqM_defaultHW_INPUT_FILTER, "Аппаратный фильтр входных импульсов. Default=0, Max=16" },
	{"hwInputPrescaler", NULL, C11N_UINT16, (setterFunction_t) freqM_hwInputPrescaler_set, (getterFunction_t) freqM_hwInputPrescaler_get, 0,0, freqM_defaultHW_INPUT_PRESCALER, "Предделитель входных импульсов. Beta. Default=0" },
	{"timfreqbase"     , NULL, C11N_UINT32, (setterFunction_t)      freqM_timfreqbase_set, (getterFunction_t)      freqM_timfreqbase_get, 0,0,  freqM_defaultTIM_REF_BASE_FREQ, "Базовая частота эталонного таймера. Max=168000000, Default=" },
	{"sampleRate"      , NULL, C11N_UINT16, (setterFunction_t)       freqM_sampleRate_set,      (getterFunction_t)  freqM_sampleRate_get, 0,0,        freqM_defaultSAMPLE_RATE, "Заданная частота дискретизации. Default=5000" },
};

const ModulesParameters_t freqM_CONFIGURATION = {
	"freqM", freqM_PARAMETERS, sizeofarr(freqM_PARAMETERS)
};
	
	
#define COMMAND_RESPONSE_HELP_SET	"Parameters: " 																							NEWLINE \
									"  hwInputFilter       Аппаратный фильтр входных импульсов. Default=0, Max=16"							NEWLINE \
									"  hwInputPrescaler    Предделитель входных импульсов. Beta. Default=0"									NEWLINE \
									"  timfreqbase         Базовая частота эталонного таймера. Max=168000000, Default="defaultTIM_REF_BASE_FREQ	NEWLINE \
									"  sampleRateInput     Заданная частота дискретизации. Default=5000"									NEWLINE \


/** Listener of public parameters. For debug purposes.
  * Some variables may be changed via debugger, and then peripherals 
  * will be automaticly reconfigured.
  * Usually recommended to use getters and setters.
  * @param  
  * @retval 
  */
int freqM_parameters_listener()
{	
	//uint16_t tempFilterRef = 0, tempFilterExt = 0;
	
	if( freqM_timfreqbase != timfreqbase_cached ){
		freqM_timfreqbase_set(freqM_timfreqbase);
	}
	if( freqM_hwInputFilter != hwInputFilter_cached ){
		freqM_hwInputFilter_set(freqM_hwInputFilter);
	}
	if( freqM_hwInputPrescaler != hwInputPrescaler_cached){
		freqM_hwInputPrescaler_set( freqM_hwInputPrescaler );
	}
	if( freqM_sampleRate != sampleRate_cached ){
		freqM_sampleRate_set(freqM_sampleRate);
	}
	
	return 0;
}


void freqM_timfreqbase_set( uint32_t value )
{
	// Выполнить действия по изменению предделителя(Prescaler) для Reference таймера
	freqM_RefTimer->tim->PSC = drv_tim_prescaler( freqM_RefTimer->tim, freqM_timfreqbase);;
	timfreqbase_cached = freqM_timfreqbase;
}

uint32_t freqM_timfreqbase_get( void ) 
{
	return timfreqbase_cached;
}


void freqM_hwInputFilter_set( uint16_t value )
{
	uint16_t tempFilterRef = 0, tempFilterExt = 0;

	if (value > 0xF)
		value = 0xF;
	if (value > 3) {
		// Если фильтр >3, проверить раветство ClockDivision, если не равны макс значение фильтра 3.
		if( (freqM_RefTimer->tim->CR1 & TIM_CR1_CKD) != (freqM_RefTimer->tim->CR1 & TIM_CR1_CKD) ){
			value = 3;
		}
	}
	tempFilterRef = timFiltersTable[value][1];
	tempFilterExt = timFiltersTable[value][0];
	
	// Изменить входной фильтр для TimExtCounter.ETR и TimRef.inCap1
	freqM_RefTimer->tim->CCMR1 &= ~TIM_CCMR1_IC1F;	// Очистить биты фильтра inCap1
	freqM_RefTimer->tim->CCMR1 |= (uint16_t)(tempFilterRef << 4);	// Установить фильтр
	freqM_ExtCounter->tim->SMCR  &= ~TIM_SMCR_ETF;	// Очистить биты фильтра ETR 
	freqM_ExtCounter->tim->SMCR  |= (uint16_t)(tempFilterExt << 8);	// Установить фильтр
	
	freqM_hwInputFilter = hwInputFilter_cached = value;
}

uint16_t freqM_hwInputFilter_get( void ) {
	return hwInputFilter_cached;
}


void freqM_hwInputPrescaler_set( uint16_t value )
{
	value = value > 3 ? 3 : value;
	
	// Задать значения предделителей
	freqM_RefTimer->tim->CCMR1 &= ~TIM_CCMR1_IC1PSC; 	// Очистить биты предделителя
	freqM_RefTimer->tim->CCMR1 |= value << 2;			// альтернатива TIM_SetIC1Prescaler();
	freqM_ExtCounter->tim->SMCR &= ~TIM_SMCR_ETPS;		// Очистить биты предделителя ETR
	freqM_ExtCounter->tim->SMCR |= value << 12;
	
	freqM_hwInputPrescaler = hwInputPrescaler_cached = value;
}

uint16_t freqM_hwInputPrescaler_get( void ){
	return hwInputPrescaler_cached;
}


void freqM_sampleRate_set( uint16_t value )
{
	uint32_t period;
	
	period = freqM_timfreqbase / freqM_sampleRate;
	sampleRatePeriod = period > 0xFFFF ? 0xFFFF : period;
	
	freqM_sampleRate = freqM_timfreqbase / sampleRatePeriod;
	freqM_RefTimer->tim->ARR = sampleRatePeriod;
	sampleRate_cached = freqM_sampleRate;
	//TODO изменить коэф. k который зависит от частоты дискретизации
}

uint16_t freqM_sampleRate_get( void ){
	return sampleRate_cached;
}
