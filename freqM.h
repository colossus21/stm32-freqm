/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/** 
 * @author  Ivan Кувалда(Sledgehammer) Kuvaldin <mailto:i.kyb@ya.ru>
 * @brief	Модуль предназначен для определения частоты входного дискретного сигнала
 */

#ifndef __freqM_H
#define __freqM_H

#ifdef __cplusplus
extern "C" {
#endif


#include "freqM_Config.h"  /// take example and save it in your project config dir
#include "Sledge/drv/stm32/sledge_stm32_gpio.h"
#include "Sledge/drv/stm32/sledge_stm32_timers.h"


/// Проверка конфигурационного файла.
//#if !defined(freqM_RefTimer) || !defined(freqM_ExtCounter)
// #error freqM_RefTimer and freqM_ExtCounter must be defined.
//#endif



//typedef void (*freqM_Callback_t)(float freq, void *params);
typedef struct freqM_Callback_t {
	void (*func)(float freq, void *params);
	void *params;
} freqM_Callback_t;

typedef struct freqM_FailCallback_t {
	void (*func)(void *params);
	void *params;
} freqM_FailCallback_t;

typedef struct freqM_Settings_t {
	const TIM_handler_t *reftimer;         /// опорный таймер
	const TIM_handler_t *refextender;      /// расширение опорного таймера
	const TIM_handler_t *counter;          /// Счётчик входных импульсов
	const TIM_handler_t *samplertimer;     /// задаёт частоту дискретизации
	
	const GPIO_t *ref_cc_pin;        /// ввод захват/сравнение опорного таймера
	const GPIO_t *cnt_etr_pin;       /// ввод внешний триггер счётчика

	unsigned reftimer_base_freq;     /// базовая частота опорного таймера
									 /** Базовая частота таймеров. Может быть использовано для понижения эталонной частоты 
									  * частото-измерительного модуля во время выполнения. 
									  * Пременные с суффиксом _cached используются функцией @ref parameters_listener() для
									  * отслеживания изменения соответствующих переменных извне (напр. отладчика или CLI) 
	                                  */
	unsigned window_size;            /// Размер окна измерений
	unsigned sample_rate_freq;       /// частота дискретизации

	uint8_t hwInputFilter;     /// Уровень входного аппаратного фильтра (кол-во опорных тактов - время фильтрации)
						       /** Аппаратный фильтр таймеров STM32. Подробности в Ref.Man.p.614 "External trigger filter"
								 * @note Тут нужно учесть, что частота сэмплирования фильтра завистит от базовой частоты 
								 * таймера и если значение фильтра больше 0x3, то еще от TIM_ClockDivision. 
								 * Т.е если TIM4, сидящий на APB1, имеет Fck_int=84MHz и фильтр 1(или 2), то для TIM1, 
								 * работающего на частоте Fck_int=168MHz фильтр должен быть 2(или 3) соответственно 	
	                             */	
	uint16_t hwInputPrescaler; /// Входной предделитель, фактическое значение предделителя hwInputPrescaler+1
	
	uint8_t irq_prio;          /// Приоритет прерываний
	
	void(*dma_init)(void);     /// Фунцкия, настраивающая DMA для нестандартных таймеров. NULL по умолчанию.
	
	freqM_Callback_t callback; /// функция, кот. будет вызвана по окончании измерения, В ПРЕРЫВАНИИ!
	freqM_FailCallback_t fail_callback; /// функция, кот. будет вызвана при невозможности определить частоту, В ПРЕРЫВАНИИ!
} freqM_Settings_t;

#define freqM_Settings_DEFAULTS  { &TIMS[1], &TIMS[3], &TIMS[4], &TIMS[7], \
                                   PA8, PE0, \
	                               168000000, 0, 0, \
	                               2, 0, \
	                               /*irq_prio*/ 5, \
	                               NULL, \
                                   NULL, NULL \
                                 }

extern volatile freqM_Settings_t freqM_settings;


/** Базовая частота таймеров. Может быть использовано для понижения эталонной частоты 
  * частото-измерительного модуля во время выполнения. 
  * Пременные с суффиксом _cached используются функцией @ref parameters_listener() для
  * отслеживания изменения соответствующих переменных извне (напр. отладчика или CLI) */
//extern /*volatile*/ uint32_t freqM_timfreqbase;															

/** Аппаратный фильтр таймеров STM32. Подробности в Ref.Man.p.614 "External trigger filter"
  * @note Тут нужно учесть, что частота сэмплирования фильтра завистит от базовой частоты 
  * таймера и если значение фильтра больше 0x3, то еще от TIM_ClockDivision. 
  * Т.е если TIM4, сидящий на APB1, имеет Fck_int=84MHz и фильтр 1(или 2), то для TIM1, 
  * работающего на частоте Fck_int=168MHz фильтр должен быть 2(или 3) соответственно 	*/	
//extern /*volatile*/ uint16_t freqM_hwInputFilter;	// Значение для TimExtCounter.SMCR.ETF и TimRef.inCap1.filter

/// Предделитель входного сигнала (импульсов)
//extern /*volatile*/ uint16_t freqM_hwInputPrescaler;

// Частота дискретизации, период дискретизации
//extern /*volatile*/ uint16_t freqM_sampleRate;



/// Инициализация переферии freqM
int freqM_init( freqM_Settings_t *s );

/// Сброс переферии freqM. Полный останов и освобожление занятых переферийных модулей и портов
void freqM_deinit(void);

/**
 * Функция предназначена для отслеживания изменений некоторых 
 * параметров ЧИМ(частото-измерительного модуля или FMM) freqM.
 * И вносит соответствующие изменения в регистры таймеров.
 */
int freqM_parameters_listener(void);

/**
 * timfreqbase - частота опорного таймера
 */
void freqM_timfreqbase_set( uint32_t value );
uint32_t freqM_timfreqbase_get( void );

/**
 * hwInputFilter - определяет аппаратный фильтр входного сигнала
 */
void     freqM_hwInputFilter_set( uint16_t value );
uint16_t freqM_hwInputFilter_get(void);

/**
 * hwInputPrescaler - определяет аппаратный предделитель входного сигнала (импульсов)
 */
void     freqM_hwInputPrescaler_set( uint16_t value );
uint16_t freqM_hwInputPrescaler_get(void);

/**
 * sampleRateInput - частота дискретизации частотомера
 */
void freqM_sampleRate_set( uint16_t value );
uint16_t freqM_sampleRate_get();


/**
 *
 */
inline static void freqM_start(){
	// Сбросить счетные регистры таймеров (Сгенерировать программно событие Update)
	freqM_settings.reftimer->tim->EGR = TIM_EGR_UG;  //freqM_RefTimer  ->tim->EGR = TIM_EGR_UG;
	freqM_settings.counter ->tim->EGR = TIM_EGR_UG;  //freqM_ExtCounter->tim->EGR = TIM_EGR_UG;
	// Можно работать дальше - включить автостарт - slave mode счетчика внешних импульсов
	freqM_settings.counter->tim->SMCR |= TIM_SlaveMode_Trigger;  //freqM_ExtCounter->tim->SMCR |= TIM_SlaveMode_Trigger;
}
///\todo freqM_start_once()
///\todo freqM_start_continious()

/**
 *
 */
inline static void freqM_stop(){
	// отключить автостарт, будет запущен в прерывании от sample_rate таймера, или если не нужно точной дискретизации после окончания расчётов
	freqM_settings.counter->tim->SMCR &= ~TIM_SMCR_SMS;	// Отключить slave mode у счетчика внешних импульсов.
	// Остановить таймеры.
	freqM_settings.reftimer->tim->CR1 &= ~TIM_CR1_CEN;
	freqM_settings.counter ->tim->CR1 &= ~TIM_CR1_CEN;
	
	// Сбросить флаг, запретить прерывание.
	freqM_settings.reftimer->tim->SR &= ~TIM_SR_CC1IF;     //freqM_RefTimer->tim->SR &= ~TIM_SR_CC1IF;		// Сбросить флаг прерывания CC1 опорного таймера
	freqM_settings.reftimer->tim->DIER &= ~TIM_DIER_CC1IE; //freqM_RefTimer->tim->DIER &= ~TIM_DIER_CC1IE;	// Запретить прерывание CC1 опорного таймера. Будет разрешено позже по DMA.
}



#ifdef __cplusplus
}
#endif

#endif //__freqM_H
