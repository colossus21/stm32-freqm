/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/** 
 * @author  Ivan Кувалда(Sledgehammer) Kuvaldin <mailto:i.kyb@ya.ru>
 * @brief   This is the configuration file for Frequency Measurement Module (FMM)
 *          Здесь находятся константы, параметры и ограничения времени компиляции.
 */

#ifndef __freqM_Config_H
#define __freqM_Config_H


/// ПАРАМЕТРЫ ЧАСТОТО-ИЗМЕРИТЕЛЬНОГО МОДУЛЯ по умолчанию
/// Choose the prefered frequency measurement mode used by default. REGULAR==1 or RECIPROCAL==2 
//#define freqM_defaultMEASUREMENT_MODE   2
//#define freqM_defaultTIM_REF_BASE_FREQ	168000
//#define freqM_defaultSAMPLE_RATE        2	 /// Частота дискретизации максимальная по умолчанию
//#define freqM_defaultMIN_INPUT_FREQ     5   /// Наименьшая входная частота. Определяет ширину дополнительного окна

/// Назначение таймеров
#define freqM_RefTimer         (&TIMS[1])
#define freqM_ExtCounter       (&TIMS[4])
#define freqM_SampleRateTimer  (&TIMS[7])

/// Привязать функцию-обработчик прерывания по переполнению опорного таймера к действительному вектору прерывания.
///\note Как альтернатива можно вызывать эту фунцию из обработчика прерывания.
#define freqM_RefTimer_Upd_IRQHandler     TIM1_UP_IRQHandler

/// Определить функцию-обработчик прерывания по захвату/сравнению канала 1 опорного таймера
#define freqM_RefTimer_CC_IRQHandler      TIM1_CC_IRQHandler
/// Обработчик прерывания переполнения таймера, задающего дискретизацию
#define freqM_SampleRateTimer_IRQHandler  TIM7_IRQHandler

#define freqM_defaultHW_INPUT_FILTER	2	   /// Значение аппаратного фильтра Таймера STM32 по умолчанию
#define freqM_defaultHW_INPUT_PRESCALER	0	   /// Значение аппаратного предделителя входных импульсов по умолчанию


/// Ножки для ввода исследуемого сигнала
#define freqM_reftim_cc_pin    PA8
#define freqM_counter_etr_pin  PE0


/** INTERRUPTS PRIORITY. Don't touch unless you know what is this.
 *	FreqM's IRQs priorities should be the highest priority in program. 
 *	Otherwise, accuracy reduction is possible.
 */
//#define freqmIRQPrio	5		/// refer to configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY (FreeRTOS)


#endif //__freqM_Config_H
