/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/** 
 * @author  Ivan Кувалда(Sledgehammer) Kuvaldin <mailto:i.kyb@ya.ru>
 * @brief   Initialize peripherals for frequency measure module.
 *			This file contains functions to configure peribheral needed for 
 *			Frequency measurement module.
 *
 * \todo Измерение скважности
 * \todo freqM_settings.hwInputFilter - провести через проверку
 */

#include "./freqM.h"
#include "Sledge/bsp.h"
#include "Sledge/debug.h"

#include "stm32f4xx_rcc.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_dac.h"
#include "misc.h"


//volatile freqM_callback_t freqM_ready_callback = NULL;
//volatile void *freqM_callback_params = NULL;
//volatile freqM_Callback_t freqM_ready_callback = {NULL,NULL};

#define CACHED_BY_DMA 5
/** Public variables, used by DMA ---------------------------------------------*/
//volatile uint16_t CCR1mem, 
//				  CNT4mem;
volatile uint16_t CCR1memArr[CACHED_BY_DMA],
				  CNT4memArr[CACHED_BY_DMA];

/// Переменная хранит значение DIER с разрешенным прерыванием от CC опорного таймера
/// используется для загрузки по DMA в регистр таймера
static volatile uint16_t timRefCCena;


/* Private functions definitions ---------------------------------------------*/
static void configRefTimer              ( const TIM_handler_t *t, uint32_t freq );
static void configInternalTriggerCounter( const TIM_handler_t *t, uint16_t triggerSrc );
static void configExtTriggerCounter     ( const TIM_handler_t *t );
static void configSyncronisation        ( const TIM_handler_t *external, const TIM_handler_t *ref );
static void configSampleRateTimer       ( const TIM_handler_t *t, uint32_t freq, uint8_t prio );

static void configNVIC( uint8_t prio );
static void freqM_TIM1_DMA_Config(void);	/// Конфиг DMA для частотомера


volatile freqM_Settings_t freqM_settings = freqM_Settings_DEFAULTS;
//static bool freqM_configured = false;



/**
 * 
 */
int freqM_check_settings( freqM_Settings_t *s )
{
	if( s->reftimer == NULL ){
		debugf("опорный таймер должен быть таймером\n");
		return -1;
	}
	//s->refextender;      /// расширение опорного таймера
	if( s->counter == NULL ){
		debugf("Счётчик входных импульсов должен быть таймером\n");
		return -3;
	}
	//s->samplertimer;/// задаёт частоту дискретизации
	
	if( TIM_ObtainTriggerSource(s->reftimer,s->counter) == (uint16_t)-1 ){
		debugf("Неверная пара опорный таймер - счётчик внеш. имп.\n");
		return -4;
	}
	/*if( TIM_ObtainTriggerSource(s->refextender,s->reftimer) == (uint16_t)-1 ){
		debugf("Неверная пара опорный таймер - расширитель.\n");
		return -4;
	}*/
	
	if( !is_pin_belong_tim_ch( s->ref_cc_pin, s->reftimer ) ){
		debugf("Неверный ввод захват/сравнение опорного таймера\n");
		return -5;
	}
	if( !is_pin_belong_tim_etr( s->cnt_etr_pin, s->counter ) ){
		debugf("Неверный пин внешний триггер счётчика\n");
		return -6;
	}

	if( s->reftimer_base_freq > TIM_MAX_FREQUENCY_T(s->reftimer) ){
	    debugf("Неверная базовая частота опорного таймера\n");
		return -7;
	}
	//s->window_size;            /// Размер окна измерений
	if( s->sample_rate_freq > TIM_MAX_FREQUENCY_T(s->reftimer) / 1000 ){
		debugf("Неверная частота дискретизации\n");
		return -9;
	}
	
	if( s->hwInputFilter > 0xF ){
		debugf("Неверный уровень аппаратного фильтра\n");
		s->hwInputFilter = 0xF;
	}
	//s->hwInputPrescaler; /// Входной предделитель
	
	//s->irq_prio;          /// Приоритет прерываний
	
	//s->callback; /// функция, кот. будет вызвана по окончании измерения, В ПРЕРЫВАНИИ!
	//s->fail_callback; /// функция, кот. будет вызвана по окончании измерения, В ПРЕРЫВАНИИ!
	return 0;
}


/// возвращает КОПИЮ текущих настроек модуля
freqM_Settings_t freqM_getCurrentSettings(freqM_Settings_t *s){
	return freqM_settings;
}

/// Модуль freqM постарается исправить неверные настройки, 
/// структура по указателю будет изменена, и содержать текущие настройки. 
/// Также в этом случае вернётся соответствующая ошибка
///\return 0 - OK, >0 ошибка исправлена, <0 критическая ошибка, настройки отклонены.
int freqM_setSettings(freqM_Settings_t *s){
	int err = freqM_check_settings(s);
	if( err )  return err;
	///\todo
	return 0;//&current_settings;
}


/** Configure all pripherals for Frequency Measurement Module.
  */
//void freqM_init( freqM_Callback_t cb, void *params ) 
int freqM_init( freqM_Settings_t *s ) 
{
//	current_settings = *s;  // запомнить настройки
	//freqM_ready_callback = cb;
	//freqM_callback_params = params;
	
	///freqM_setSettings(s);
	if( freqM_check_settings(s) < 0 )
		return -1;
	freqM_settings = *s;
	
	GPIO_InitTypeDef gis;
	gis.GPIO_OType = GPIO_OType_PP;
	gis.GPIO_PuPd  = GPIO_PuPd_DOWN;
	gis.GPIO_Speed = GPIO_Speed_100MHz;
	
	freqM_deinit();
	
	configNVIC( s->irq_prio );
	
	// Настройка таймера задающего частоту дискретизации
	if( s->samplertimer && s->sample_rate_freq > 0 )
		configSampleRateTimer( s->samplertimer, s->sample_rate_freq, s->irq_prio );
	
	// Настройка внутреннего эталонного опорного таймера
	bsp_GpioAf_InitStruct( s->reftimer->ch_Pins[0][0], s->reftimer->af, &gis ); 	// TIM1_Ch1 PA8
	configRefTimer( s->reftimer, s->reftimer_base_freq );
	
	///\todo Таймер расширяющий окно измерения ещё на 16 бит, считает количество переполнений опорного.
	if( s->refextender )
		configInternalTriggerCounter( s->refextender, TIM_ObtainTriggerSource(s->refextender, s->reftimer) );
	
	// Счетчик входных фронтов
	bsp_GpioAf_InitStruct( freqM_settings.counter->ETR_Pins[0], freqM_settings.counter->af, &gis );  // PE0
	//bsp_GpioAf( freqM_ExtCounter->ch_Pins[1][1], freqM_ExtCounter->af );		// TIM4_Ch2 PE7
	configExtTriggerCounter( freqM_settings.counter );		// Cчитать вх. фронты
	
	// Настроить синхронизацию опорного таймера, и счётчика импульсов
	configSyncronisation( freqM_settings.counter, freqM_settings.reftimer );
	
	// OLD. TIM4 registers used to control its behavour throught DMA-requests
	//extCounterWorking = TIMS[4].tim->SMCR;
	//extCounterStopped = extCounterWorking & ~TIM_SMCR_SMS;	// SMCR.SMS=0. Reset Slave mode.
	
	// Настроить DMA
	if( s->dma_init ) s->dma_init();
	else freqM_TIM1_DMA_Config();
	
	// Сохранить в переменную регистр DIER с разрешением прерывания от 1го канала, для последующего заполнения по DMA
	timRefCCena = freqM_settings.reftimer->tim->DIER | TIM_DIER_CC1IE; 	// Сохранить для DMA
	// Включить DMA по запросу TIM1_Upd, DMA разрешит прерывание прерывание от канала 1
	DMA_Cmd(DMA2_Stream5, ENABLE);		// Включить DMA по запросу TIM1_Upd

//	freqM_settings = *s;
//	freqM_configured = true;
	return 0;
}


/**
 * Вернуть перефирию в исходное состояние
 */
void freqM_deinit()
{
	if(freqM_settings.reftimer)
		RCC_TIMx_APBxClockCmd( *freqM_settings.reftimer, DISABLE );
	
	if(freqM_settings.refextender)
		RCC_TIMx_APBxClockCmd( *freqM_settings.refextender, DISABLE );
	
	if(freqM_settings.counter)
		RCC_TIMx_APBxClockCmd( *freqM_settings.counter, DISABLE );
	///\todo DMA deinit
	///\todo diag pins deinit
}


/** 
 * Настроить таймер как источник опорной частоты для частотомера.
 * @param t timer handler
 * @param freq  Base frequency of TIM counter. 168000000 recomended for STM32F4xx.
 */
static void configRefTimer( const TIM_handler_t *t, uint32_t freq)
{
	TIM_TimeBaseInitTypeDef	timebase = TIM_TimeBaseInitStruct_Default;	// TIM_TimeBaseStructInit(&timebase); 
	TIM_ICInitTypeDef		incap;	// Input Capture management structure
	TIM_OCInitTypeDef		outcap;	// Output Capture management structure
	//TIM_BDTRInitTypeDef		bdtr;
//	uint32_t basicWindow;

	assert_amsg( freq <= TIM_MAX_FREQUENCY_T(t) );
	assert_amsg( !is_TIMx_in_use(t) );
	
	RCC_TIMx_APBxClockCmd( *t, ENABLE );
	TIM_DeInit( t->tim );
	
	// Time base configuration 
	timebase.TIM_Prescaler = drv_tim_prescaler(t->tim, freq);	// Предделитель. TIM1,8,9,10,11 ~ 168MHz max 
	// Period определяет ширину осн. окна reciprocal counter
	// Дополнительное окно варьируется от входной частоты 
	// Осн.окно = полное.окно - доп.окно = 1/частота_дискретизации - 1/наименьшая_входная_частота = наим_вх_ч * ч_дискр / ( наим_вх_ч - ч_дискр )
//	basicWindow = freqM_defaultMIN_INPUT_FREQ * freqM_defaultSAMPLE_RATE / (freqM_defaultMIN_INPUT_FREQ - freqM_defaultSAMPLE_RATE);
//	timebase.TIM_Period = freq / (timebase.TIM_Prescaler +1) / basicWindow; 
	if( freqM_settings.sample_rate_freq > 0 )
		timebase.TIM_Period = freq / freqM_settings.sample_rate_freq;
	else
		timebase.TIM_Period = 0xFFFFFFFF;
	
	timebase.TIM_CounterMode = TIM_CounterMode_Up;
	timebase.TIM_RepetitionCounter = 0; 	// only for advanced-control timers TIM1 and TIM8
	TIM_TimeBaseInit( t->tim, &timebase );
	
	/* TIMx configuration: Input capture mode ------------------------
	   The Rising edge is used as active edge,
	   The TIMx CCR1 is used to compute the frequency value for CH1
	   ------------------------------------------------------------ */
	TIM_ICStructInit(&incap);
	incap.TIM_ICPolarity = TIM_ICPolarity_Rising; 		// Specifies the active edge of the input signal
	incap.TIM_ICSelection = TIM_ICSelection_DirectTI/*TRC*/;	// TIM Input 1, 2, 3 or 4 is selected to be 
														// connected to IC1, IC2, IC3 or IC4, respectively
	incap.TIM_ICPrescaler = TIM_ICPSC_DIV1;		// TIM Input Capture Prescaler = no prescaler
												// Capture performed each time an edge is detected on the capture input
												// Does no effect on Slave Trigger Mode, if it is used.
	incap.TIM_ICFilter = freqM_settings.hwInputFilter; 	// Specifies the input capture filter = no filter
											// This parameter can be a number between 0x0 and 0xF
	incap.TIM_Channel = TIM_Channel_1;
	TIM_ICInit( t->tim, &incap);
	
	
	// Подключить ch2 к TI1 для генерироаня второго запроса запроса DMA. Нужно только для отладки
	incap.TIM_ICSelection = TIM_ICSelection_IndirectTI /*TRC*/;
	incap.TIM_Channel = TIM_Channel_2;
	TIM_ICInit( t->tim, &incap);
	
	// Use ch3 as output. FOR DEBUG PURPOSES
	TIM_OCStructInit(&outcap);
	outcap.TIM_OCMode = TIM_OCMode_PWM2;	// PWM1. In upcounting, output is Active as long as TIMx_CNT<TIMx_CCR1, else it is INActive. 
											// In downcounting, output is INActive as long as TIMx_CNT>TIMx_CCR1, else it is Active.
											// PWM2 is PWM1 inverted. In upcounting, output is INActive as long as TIMx_CNT<TIMx_CCR1, else it is Active. 
											// In downcounting, output is Active as long as TIMx_CNT>TIMx_CCR1, else it is INActive.
	outcap.TIM_OutputState = TIM_OutputState_Enable;
	outcap.TIM_Pulse = 0x01;
	// Channel 3 have only LIST3: TIM1-5,8
	if( IS_TIM_HAS_Channel3( t->tim )){
		TIM_OC3Init (t->tim, &outcap);
		TIM_OC3PreloadConfig(t->tim, TIM_OCPreload_Disable);
	}
		
	// Config BDTR. Only for advanced timers TIM1 and TIM8
	if( IS_TIM_LIST_ADVANCED_CONTROL(t->tim) ){
		//TIM_BDTRStructInit(&bdtr);
		TIM_CtrlPWMOutputs( t->tim, ENABLE );
	}
	
	t->tim->CR1 |= TIM_CR1_URS;
	
 #if (defaultMEASUREMENT_MODE == 1)	// REGULAR mode used
	TIM_SelectOnePulseMode( t->tim, TIM_OPMode_Single );
	TIM_ClearITPendingBit( t->tim, TIM_IT_Update );
	TIM_ITConfig( t->tim, TIM_IT_Update, ENABLE);
 #endif
	
	TIM_ITConfig( t->tim, TIM_IT_Update, ENABLE);
	//TIM_Cmd(t->tim, ENABLE); 	// TIMx enable counting
}


/** @brief  Configure TIMx slave mode to count input triggers.
  *			Подсчет внутренних импульсов, например для внутренней связки таймеров
  * @param  timhandler	The timer. 
  * @param  triggerSrc	The trigger source selection. 
  */
static void configInternalTriggerCounter( const TIM_handler_t *t, uint16_t triggerSrc)
{
	TIM_TimeBaseInitTypeDef	timebase;
	
	assert_amsg( !is_TIMx_in_use(t) );
	assert_amsg(IS_TIM_TRIGGER_SELECTION(triggerSrc));
	
	// Запуск периферии (тактирование)
	RCC_TIMx_APBxClockCmd( *t, ENABLE );
	TIM_DeInit( t->tim );  // Сброс таймера
	
	// Time base configuration 
	TIM_TimeBaseStructInit(&timebase); 
	timebase.TIM_Period = 2563;					// Учитывая частоту таймера-источника-триггера 168Мгц, это чуть <1 сек.
	TIM_TimeBaseInit( t->tim, &timebase );
	
	// Syncronization parameters
	//TIM_SelectOutputTrigger(t->tim, TIM_TRGOSource_Update);	// Выдать Trigger по событию Update.
	TIM_SelectInputTrigger( t->tim, triggerSrc);				// Например, ITR2 для TIM9 это TIM10
	TIM_SelectSlaveMode( t->tim, TIM_SlaveMode_External1 );		// Режим счета вх. импульсов.
	TIM_SelectMasterSlaveMode( t->tim, TIM_MasterSlaveMode_Enable);	// p553 Ref.Man.
	
	// Enable the Update Interrupt Request
	TIM_ClearITPendingBit( t->tim, TIM_IT_Update );
	TIM_ITConfig( t->tim, TIM_IT_Update, ENABLE);
	
	TIM_Cmd( t->tim, ENABLE );
}


/** @brief  Configure TIMx slave mode to count external triggers on ETR pin.
  * 		@note You need to configure ETR pin alternative function first.
  * @param  timhandler	Timer handler
  * 		@note Only TIMs with external trigger input (from LIST3) avaliable. They are TIM1-5,8;
  * @param  triggerSrc	Trigger source selection. 
  */
static void configExtTriggerCounter( const TIM_handler_t *t )
{
TIM_TimeBaseInitTypeDef	timebase;
	
	assert_amsg( !is_TIMx_in_use(t) );
	assert_amsg( IS_TIM_HAS_EXTERNAL_TRIGGER_PIN(t->tim) );
	
	// Запуск периферии
	RCC_TIMx_APBxClockCmd(*t, ENABLE);
	TIM_DeInit( t->tim ); // Сброс конфигурации таймера
	
	// External clock source mode 2	/// Cчетик CNT тактируется от ETR, prescaler выкл, по переднему фронту
	TIM_ETRClockMode2Config( t->tim, TIM_ExtTRGPSC_OFF, TIM_ExtTRGPolarity_NonInverted, freqM_settings.hwInputFilter); 
	// Time base configuration 
	TIM_TimeBaseStructInit(&timebase); 
	timebase.TIM_Period = 0xFFFF;
	TIM_TimeBaseInit( t->tim, &timebase );
	
 // for DEBUG. Для ОТЛАДКИ. Use ch3 as output
 #if freqm_USE_DEBUG
	TIM_OCInitTypeDef outcap; TIM_OCStructInit(&outcap);	/// Output Capture management structure
	bsp_GpioAf( freqM_settings->reftimer->ch_Pins[2][0], freqM_settings->reftimer->af ); 	/// TIM1_Ch3 PA10
	outcap.TIM_OCMode = TIM_OCMode_PWM1;	// PWM1. In upcounting, output is Active as long as TIMx_CNT<TIMx_CCR1, else it is INActive. 
											// In downcounting, output is INActive as long as TIMx_CNT>TIMx_CCR1, else it is Active.
											// PWM2 is PWM1 inverted. In upcounting, output is INActive as long as TIMx_CNT<TIMx_CCR1, else it is Active. 
											// In downcounting, output is Active as long as TIMx_CNT>TIMx_CCR1, else it is INActive.
	outcap.TIM_OutputState = TIM_OutputState_Enable;
	outcap.TIM_Pulse = 4;
	TIM_OC2Init (t->tim, &outcap);
	TIM_OC2PreloadConfig(t->tim, TIM_OCPreload_Disable);
	// Channel 3 have only LIST3: TIM1-5,8
	if (IS_TIM_LIST3_PERIPH( t->tim )) {
		TIM_OC3Init (t->tim, &outcap);
		TIM_OC3PreloadConfig(t->tim, TIM_OCPreload_Disable);
	}
 #endif
	
	// Config BDTR. Only for TIM1 and TIM8.
	if( IS_TIM_LIST_ADVANCED_CONTROL(t->tim) ){
		//TIM_BDTRStructInit(&bdtr);
		TIM_CtrlPWMOutputs( t->tim, ENABLE );
	}
	
	TIM_SelectOnePulseMode( t->tim, TIM_OPMode_Single );
	
	// Enable the CC1 Interrupt Request
	//TIM_ITConfig(t->tim, TIM_IT_CC1 /*| TIM_IT_Update*/, ENABLE);
	
	//TIM_Cmd(t->tim, ENABLE);
}


/** 
 * Синхронизация таймеров. В данный момент привязано к TIM1+TIM4.
 * timExternalCounter(TIM4) запускается по входному фронту ETRF, 
 * и выдаёт Enable в качестве выходного триггера. 
 * timRef(TIM1) получает входной триггер от TIM4 (или внешний TI1FP1),
 * и выдаёт триггер по событию Update, это нужно для счета при интервалах
 *	больших, чем период TIM1
 * @param  thExternalCounter  счётчик входных импульсов
 * @param  thRef  опрный таймер
 * @param  triggerSrc  источник триггера TODO
 */
static void configSyncronisation( const TIM_handler_t *external, const TIM_handler_t *ref )//, uint16_t triggerSrc) 
{	
	// External pulses Counter Syncronization parameters
	TIM_SelectInputTrigger( external->tim, TIM_TS_ETRF /*triggerSrc*/);				
	TIM_SelectSlaveMode( external->tim, TIM_SlaveMode_Trigger /* Gated*/ );
	TIM_SelectOutputTrigger( external->tim, TIM_TRGOSource_Enable /*OC2Ref*/ );	// Выдать Trigger по событию включение таймера, т.е. сразу
	TIM_SelectMasterSlaveMode( external->tim, TIM_MasterSlaveMode_Enable);	// p553 Ref.Man.
	
	// Reference timer Sync params
	TIM_SelectInputTrigger( ref->tim, TIM_ObtainTriggerSource(ref,external) );
	TIM_SelectSlaveMode( ref->tim, TIM_SlaveMode_Trigger );		// Запускаться по триггеру
	TIM_SelectOutputTrigger( ref->tim, TIM_TRGOSource_Update );	// Выдать Trigger по переполнениню (Update).
	TIM_SelectMasterSlaveMode( ref->tim, TIM_MasterSlaveMode_Enable);	// p553 Ref.Man.
}


/** 
 * Настроить таймер, задающий частоту дискретизации. 
 * Параметры Period, Prescaler, ClockDivision будут выбраны автоматически
 * для обеспечения макс. детализации
 * @param 	timhandler	Таймер STM32, описаный структорой TIM_handler_t из Sledge.
 *						Рекомендуется TIMS[6] или TIMS[7].
 * @param 	updFreq		Частота, с которой будут происходить события переполнения (Update).
 */
static void configSampleRateTimer( const TIM_handler_t *t, uint32_t updFreq, uint8_t prio )
{	
	TIM_TimeBaseInitTypeDef	timebase = TIM_TimeBaseInitStruct_Default;	//TIM_TimeBaseStructInit(&timebase); 
	NVIC_InitTypeDef nvic; 		// Структура для конфигурации регистров прерываний
	
	assert_amsg( !is_TIMx_in_use(t) );
	assert_amsg( updFreq <= TIM_MAX_FREQUENCY_T(t) );
	
	RCC_TIMx_APBxClockCmd( *t, ENABLE);
	TIM_DeInit( t->tim);
	
/*	// Time base configuration 
	//timebase.TIM_ClockDivision = TIM_CKD_DIV1;
	///\TODO механизм вычисления Prescaler и Period. Prescaler должен быть минимальным. Так точнее
	timebase.TIM_Prescaler = 2-1; //drv_tim_prescaler(t->tim, freq);
	timebase.TIM_Period = 84000000 / (timebase.TIM_Prescaler+1) / freqM_defaultSAMPLE_RATE -1;
	//timebase.TIM_CounterMode = TIM_CounterMode_Up;
	//timebase.TIM_RepetitionCounter = 0; 	// only for advanced-control timers TIM1 and TIM8
	TIM_TimeBaseInit( t->tim, &timebase );
*/	
	drv_tim_AutoFillTimeBase( t, updFreq, &timebase );
	TIM_TimeBaseInit( t->tim, &timebase );
	
	// Настроить прерывание по переполнению.
	TIM_ClearITPendingBit( t->tim, TIM_IT_Update );
	TIM_ITConfig( t->tim, TIM_IT_Update, ENABLE);
	
	// Настроить контроллер прерываний
	nvic.NVIC_IRQChannel = t->updIrqN;
	nvic.NVIC_IRQChannelPreemptionPriority = prio;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);

	TIM_Cmd(t->tim, ENABLE); 	// Enable counting
}


/** @brief  Configure the NVIC for frequency measure module.
  * @param  prio	The value to set priority 
  * 	@note	This function uses only preemption priority.
  * 	@note 	If you use FreeRTOS refer to configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY
  * 			or analog in other RTOS
  */
static void configNVIC( uint8_t prio )
{
	NVIC_InitTypeDef nvic; // Структура для конфигурации регистров прерываний
		
	// Enable and set reference TIM update interrupt, and may be some other TIM global
	nvic.NVIC_IRQChannel = freqM_settings.reftimer->updIrqN;
	nvic.NVIC_IRQChannelPreemptionPriority = prio;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);
	// Enable and set reference TIM capture/compare interrupt
	nvic.NVIC_IRQChannel = TIM1_CC_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = prio;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);	
	
	// Enable and set TIM3 any interrupt
	/*nvic.NVIC_IRQChannel = TIM3_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 1;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);
	*/
	/*// Enable and set TIM4 any interrupt
	nvic.NVIC_IRQChannel = TIM4_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);
	*/
}


/// Наибольшей проблемой и трудностью стала невозможность синхронной остановки таймеров.
/// Поэтому используется режим синхронизации ТРИГГЕР.
/// А остановка путём аппаратной отправки запроса DMA и разрешения прерывания СС1 опорного таймера.

/** TIM1 CC1 with DMA configuration.
  * Копирование регистров TIM1.CCR1, TIM4.CNT в соответствующие переменные
  *	в момент события захвата TIM1_CC1 и TIM1_CC2.
  * Нет необходимости. Теперь для ОТЛАДКИ.
  *  @note	ЖЕСТКАЯ ПРИВЯЗКА к TIM1
  */
static void freqM_TIM1_DMA_Config(void)
{
	DMA_InitTypeDef dis; DMA_StructInit(&dis);  // create and fill with defaults

	// Enable DMA1, DMA2 clocks
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
	
	// Сохранение TIM1->CCR1 в память по событию захвата TIM1_СС1 || DMA ch6 str1
	dis.DMA_Channel = DMA_Channel_6;
	dis.DMA_DIR = DMA_DIR_PeripheralToMemory;
	dis.DMA_PeripheralBaseAddr = (uint32_t)&(TIM1->CCR1);
	dis.DMA_Memory0BaseAddr = (uint32_t)&CCR1memArr[0];
	dis.DMA_BufferSize = CACHED_BY_DMA;//1;
	dis.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	dis.DMA_MemoryInc = DMA_MemoryInc_Enable;
	dis.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	dis.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	dis.DMA_Mode = DMA_Mode_Circular;
	dis.DMA_Priority = DMA_Priority_VeryHigh;
	dis.DMA_FIFOMode = DMA_FIFOMode_Disable;				// Direct mode used.
	//dis.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;   // Doesn't metter while Direct mode used (FIFO disabled)
	//dis.DMA_MemoryBurst = DMA_MemoryBurst_Single;         // Doesn't metter while Memory Increment mode disabled
	//dis.DMA_PeripheralBurst = DMA_PeripheralBurst_Single; // Doesn't metter while Peripheral Increment mode disabled
	
	DMA_Init(DMA2_Stream1, &dis);
	DMA_Cmd(DMA2_Stream1, ENABLE);
	// Сonfigure & enable DMA request from TIM1_CC1
	TIM_DMAConfig (TIM1, TIM_DMABase_CCR1, TIM_DMABurstLength_1Transfer);
	TIM_DMACmd (TIM1, TIM_DMA_CC1, ENABLE);
	
	
// #if freqm_USE_DEBUG
	// Сохранение TIM4->CNT в память по событию захвата TIM1_СС2 || DMA ch6 str3 при тех же параметрах
	dis.DMA_Channel = DMA_Channel_6;  
	dis.DMA_PeripheralBaseAddr = (uint32_t) &(TIM4->CNT);
	dis.DMA_Memory0BaseAddr = (uint32_t) &CNT4memArr[0];//&CNT4mem;
	dis.DMA_BufferSize = CACHED_BY_DMA;//1;
	dis.DMA_MemoryInc = DMA_MemoryInc_Enable;//Disable;
	
	DMA_Init(DMA2_Stream2, &dis);
	DMA_Cmd(DMA2_Stream2, ENABLE);
	// Сonfigure & enable DMA request from TIM1_CC2
	TIM_DMAConfig (TIM1, TIM_DMABase_CNT/*CCR2*/, TIM_DMABurstLength_1Transfer);
	TIM_DMACmd (TIM1, TIM_DMA_CC2, ENABLE);
	
	
	// Разрешить прерывание TIM1_CC1 по DMA-запросу TIM1_Upd. ch6, stream5
	dis.DMA_Channel = DMA_Channel_6;
	dis.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	dis.DMA_Memory0BaseAddr = (uint32_t)&timRefCCena;
	dis.DMA_PeripheralBaseAddr = (uint32_t)&(TIM1->DIER);
	dis.DMA_BufferSize = 1;	
	dis.DMA_MemoryInc = DMA_MemoryInc_Disable;
	
	DMA_Init(DMA2_Stream5, &dis);
//	DMA_Cmd(DMA2_Stream5, ENABLE);
	// Сonfigure & enable DMA request from TIM1_UPD
//	TIM_DMAConfig (TIM1, TIM_DMABase_CCR1, TIM_DMABurstLength_1Transfer);
	TIM_DMACmd (TIM1, TIM_DMA_Update, ENABLE);
// #endif


/*	// ОТКЛЮЧЕНО. Авто стоп измерительного модуля по TIM3_Upd
	dis.DMA_Channel = DMA_Channel_5;
	dis.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	dis.DMA_Memory0BaseAddr = (uint32_t) &timerOff_CR1;
	dis.DMA_PeripheralBaseAddr = (uint32_t) &(TIM4->CR1);
	dis.DMA_BufferSize = 1;
	dis.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	dis.DMA_MemoryInc = DMA_MemoryInc_Disable;
	dis.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	dis.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	dis.DMA_Mode = DMA_Mode_Circular;
	dis.DMA_Priority = DMA_Priority_VeryHigh;
	dis.DMA_FIFOMode = DMA_FIFOMode_Disable;         		// Direct mode used.
	//dis.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;	// Doesn't metter while Direct mode used (FIFO disabled)
	//dis.DMA_MemoryBurst = DMA_MemoryBurst_Single;			// Doesn't metter while Memory Increment mode disabled
	//dis.DMA_PeripheralBurst = DMA_PeripheralBurst_Single; // Doesn't metter while Peripheral Increment mode disabled
	
	DMA_Init( DMA1_Stream2, &dis);
	DMA_Cmd(  DMA1_Stream2, ENABLE);
	// Сonfigure & enable DMA request from TIM3_Upd
	TIM_DMAConfig (TIM3, TIM_DMABase_CR1, TIM_DMABurstLength_1Transfer);		// По идее не используется
//	TIM_DMACmd (TIM3, TIM_DMA_Update, ENABLE);	
*/	
	
/*	// ОТКЛЮЧЕНО. Авто стоп измерительного модуля по TIM4_CC2
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
	
	dis.DMA_Channel = DMA_Channel_2;
	dis.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	dis.DMA_Memory0BaseAddr = (uint32_t) &extCounterStopped;
	dis.DMA_PeripheralBaseAddr = (uint32_t) &(TIM4->SMCR);
	dis.DMA_BufferSize = 1;
	dis.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	dis.DMA_MemoryInc = DMA_MemoryInc_Disable;
	dis.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	dis.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
	dis.DMA_Mode = DMA_Mode_Circular;
	dis.DMA_Priority = DMA_Priority_VeryHigh;
	dis.DMA_FIFOMode = DMA_FIFOMode_Disable;         		// Direct mode used.
	//dis.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;	// Doesn't metter while Direct mode used (FIFO disabled)
	//dis.DMA_MemoryBurst = DMA_MemoryBurst_Single;			// Doesn't metter while Memory Increment mode disabled
	//dis.DMA_PeripheralBurst = DMA_PeripheralBurst_Single; // Doesn't metter while Peripheral Increment mode disabled
	
	DMA_Init( DMA1_Stream3, &dis);
	DMA_Cmd(  DMA1_Stream3, ENABLE);
	// Сonfigure & enable DMA request from TIM3_Upd
	//TIM_DMAConfig (TIM4, TIM_DMABase_SMCR, TIM_DMABurstLength_1Transfer);		// По идее не используется
//	TIM_DMACmd (TIM4, TIM_DMA_CC2, ENABLE);	
*/
}

/** TIM1 CC1 with DMA configuration.
  * Копирование регистров TIM1.CCR1, TIM4.CNT в соответствующие переменные
  *	в момент события захвата TIM1_CC1 и TIM1_CC2.
  * Нет необходимости. Теперь для ОТЛАДКИ.
  *  @note	ЖЕСТКАЯ ПРИВЯЗКА к TIM1
  */
/*static void freqM_TIM1_DMA_Config(void)
{
	DMA_InitTypeDef DMA_InitStructure; DMA_StructInit(&DMA_InitStructure);

	// Enable DMA1, DMA2 clocks
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
	
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;				// Direct mode used.
	//DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;   // Doesn't metter while Direct mode used (FIFO disabled)
	//DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;         // Doesn't metter while Memory Increment mode disabled
	//DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single; // Doesn't metter while Peripheral Increment mode disabled
	
	// Разрешить прерывание TIM1_CC1 по DMA-запросу TIM1_Upd. ch6, stream5
	DMA_InitStructure.DMA_Channel = DMA_Channel_6;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&timRefCCena;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(TIM1->DIER);
	DMA_InitStructure.DMA_BufferSize = 1;	
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
	
	DMA_Init(DMA2_Stream5, &DMA_InitStructure);
	//DMA_Cmd(DMA2_Stream5, ENABLE);
	// Сonfigure & enable DMA request from TIM1_UPD
	//TIM_DMAConfig( TIM1, TIM_DMABase_CCR1, TIM_DMABurstLength_1Transfer );
	TIM_DMACmd (TIM1, TIM_DMA_Update, ENABLE);


 #if freqm_USE_DEBUG
	// Сохранение TIM1->CCR1 в память по событию захвата TIM1_СС1 || DMA ch6 str1
	DMA_InitStructure.DMA_Channel = DMA_Channel_6;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(TIM1->CCR1);
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)CCR1memArr;
	DMA_InitStructure.DMA_BufferSize = CACHED_BY_DMA;//1;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	
	DMA_Init(DMA2_Stream1, &DMA_InitStructure);
	DMA_Cmd(DMA2_Stream1, ENABLE);
	// Сonfigure & enable DMA request from TIM1_CC1
	TIM_DMAConfig (TIM1, TIM_DMABase_CCR1, TIM_DMABurstLength_1Transfer);
	TIM_DMACmd (TIM1, TIM_DMA_CC1, ENABLE);
	
	// Сохранение TIM4->CNT в память по событию захвата TIM1_СС2 || DMA ch6 str3 при тех же параметрах
	DMA_InitStructure.DMA_Channel = DMA_Channel_6;  
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(TIM4->CNT);
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)CNT4memArr;//&CNT4mem;
	DMA_InitStructure.DMA_BufferSize = CACHED_BY_DMA;//1;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;//Disable;
	
	DMA_Init(DMA2_Stream2, &DMA_InitStructure);
	DMA_Cmd(DMA2_Stream2, ENABLE);
	// Сonfigure & enable DMA request from TIM1_CC2
	TIM_DMAConfig (TIM1, TIM_DMABase_CNT/CCR2/, TIM_DMABurstLength_1Transfer);
	TIM_DMACmd (TIM1, TIM_DMA_CC2, ENABLE);
 #endif	

/	// ОТКЛЮЧЕНО. Авто стоп измерительного модуля по TIM3_Upd
	DMA_InitStructure.DMA_Channel = DMA_Channel_5;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &timerOff_CR1;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(TIM4->CR1);
	DMA_InitStructure.DMA_BufferSize = 1;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;         		// Direct mode used.
	//DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;	// Doesn't metter while Direct mode used (FIFO disabled)
	//DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;			// Doesn't metter while Memory Increment mode disabled
	//DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single; // Doesn't metter while Peripheral Increment mode disabled
	
	DMA_Init( DMA1_Stream2, &DMA_InitStructure);
	DMA_Cmd(  DMA1_Stream2, ENABLE);
	// Сonfigure & enable DMA request from TIM3_Upd
	TIM_DMAConfig (TIM3, TIM_DMABase_CR1, TIM_DMABurstLength_1Transfer);		// По идее не используется
	//TIM_DMACmd (TIM3, TIM_DMA_Update, ENABLE);	
/	
	
/	// ОТКЛЮЧЕНО. Авто стоп измерительного модуля по TIM4_CC2
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
	
	DMA_InitStructure.DMA_Channel = DMA_Channel_2;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &extCounterStopped;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(TIM4->SMCR);
	DMA_InitStructure.DMA_BufferSize = 1;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;         		// Direct mode used.
	//DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;	// Doesn't metter while Direct mode used (FIFO disabled)
	//DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;			// Doesn't metter while Memory Increment mode disabled
	//DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single; // Doesn't metter while Peripheral Increment mode disabled
	
	DMA_Init( DMA1_Stream3, &DMA_InitStructure);
	DMA_Cmd(  DMA1_Stream3, ENABLE);
	// Сonfigure & enable DMA request from TIM3_Upd
	//TIM_DMAConfig (TIM4, TIM_DMABase_SMCR, TIM_DMABurstLength_1Transfer);		// По идее не используется
	//TIM_DMACmd (TIM4, TIM_DMA_CC2, ENABLE);	
/
}*/
